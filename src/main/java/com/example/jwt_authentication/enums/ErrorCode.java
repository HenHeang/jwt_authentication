package com.example.jwt_authentication.enums;

import org.springframework.http.HttpStatus;

public enum ErrorCode {
    ACCOUNT_STATUS_CHECKER(HttpStatus.FORBIDDEN, ""),

    // 403 Forbidden
    FORBIDDEN_TRANSACTION(HttpStatus.FORBIDDEN, "Requested transaction is not yours"),

    //400 Bad Request
    BAD_REQUESTS(HttpStatus.BAD_REQUEST,"Bad request"),

    INVALID_CODE(HttpStatus.BAD_REQUEST, "Invalid two-factor code"),

    //404 Not Found
    TUTORIAL_NOT_FOUND(HttpStatus.NOT_FOUND, "Tutorial not found"),
    USER_NOT_FOUND(HttpStatus.NOT_FOUND, "User not found"),
    ROLE_NOT_FOUND(HttpStatus.NOT_FOUND, "Role not found"),

    APPLICATION_NOT_FOUND(HttpStatus.NOT_FOUND, "Application not found"),

    IMAGE_CANNOT_BE_EMPTY(HttpStatus.BAD_REQUEST,"Image cannot be empty"),

    FILE_SIZE_LIMIT(HttpStatus.BAD_REQUEST, "Maximum upload size"),

    //409 Conflict
    USER_COMPANY_EXISTS(HttpStatus.CONFLICT, "User company already exists"),

    PARTICIPANT_EXIST(HttpStatus.CONFLICT, "Participant already exist"),

    USER_EXISTS(HttpStatus.CONFLICT, "User exist"),
    ROLE_EXISTS(HttpStatus.CONFLICT, "Role exist"),

    USER_EXIST(HttpStatus.CONFLICT, "You are in this application"),

    APP_NAME_EXIST(HttpStatus.CONFLICT, "Application name already exist"),

    PLEASE_INSERT_USER(HttpStatus.BAD_REQUEST, "Please insert user"),

    ONLY_HR_ROLE_CAN_INVITE_THIS_USER(HttpStatus.BAD_REQUEST, "Only HR can invite the employee with username: {0}"),

    CANNOT_INVITE_ADMIN_USER(HttpStatus.BAD_REQUEST, "{0} is admin, You cannot invite"),

    USER_WAS_LOCKED(HttpStatus.LOCKED, "User {0} was locked"),

    COMPANY_NAME_EXIST(HttpStatus.BAD_REQUEST, "Company name already exist"),

    ADMIN_NOT_APPROVE(HttpStatus.BAD_REQUEST, "Admin aren't approve you into this application");

    private String message;

    private HttpStatus status;

    ErrorCode(final HttpStatus status, final String message) {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return this.message;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
