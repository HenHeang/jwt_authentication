package com.example.jwt_authentication.enums;

import com.example.jwt_authentication.helper.GenericEnum;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum SortDirection implements GenericEnum<SortDirection, String> {
    ASC("asc"),
    DESC("desc"),
    ;

    private final String value;

    private SortDirection(String value) {
        this.value = value;
    }
    ;
    @JsonCreator
    public static SortDirection fromValue(String value) {
        for(SortDirection my: SortDirection.values()) {
            if(my.value.equals(value)) {
                return my;
            }
        }

        return null;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    @Override
    public String getLabel() {
        String label = "(no label)";

        if("asc".equals(value)) label = "ASC";
        else if("desc".equals(value)) label = "DESC";
        return label;
    }
}
