package com.example.jwt_authentication.enums;

public enum StatusCode {
    SUCCESS(200, "Success"),

    //TODO: 401 Unauthorized
    UNAUTHORIZED(401, "Unauthorized"),
//    BAD_CREDENTIALS(401, "Bad Credentials"),

    //TODO: 403 Forbidden
    FORBIDDEN(403, "Forbidden"),
    FORBIDDEN_TRANSACTION(403, "Requested transaction is not yours"),

    //TODO: 400 Bad Request
    BAD_REQUESTS(400,"Bad request"),
    COMPANY_NAME_EXIST(400, "Company name already exist"),
    INVALID_CODE(400, "Invalid two-factor code"),
    ADMIN_NOT_APPROVE(400, "Admin aren't approve you into this application"),
    PLEASE_INSERT_USER(400, "Please insert user"),
    ONLY_HR_ROLE_CAN_INVITE_THIS_USER(400, "Only HR can invite the employee with username: {0}"),
    CANNOT_INVITE_ADMIN_USER(400, "{0} is admin, You cannot invite"),
    IMAGE_CANNOT_BE_EMPTY(400,"Image cannot be empty"),
    CURRENT_PASSWORD_MUST_BE_ENCRYPTED(400, "Current password must be encrypted!"),

    FILE_SIZE_LIMIT(400, "Maximum upload size"),

    //TODO: 404 Not Found
    TUTORIAL_NOT_FOUND(404, "Tutorial not found"),
    USER_NOT_FOUND(404, "User not found"),
    ROLE_NOT_FOUND(404, "Role not found"),

    APPLICATION_NOT_FOUND(404, "Application not found"),
    FILE_NOT_FOUND(404, "File is not found"),

    //TODO: 409 Conflict
    USER_COMPANY_EXISTS(409, "User company already exists"),

    PARTICIPANT_EXIST(409, "Participant already exist"),

    USER_EXISTS(409, "User exist"),
    ROLE_EXISTS(409, "Role exist"),

    USER_EXIST(409, "You are in this application"),

    APP_NAME_EXIST(409, "Application name already exist"),

    //TODO: 423 Locked
    USER_WAS_LOCKED(423, "User account is locked"),

    // TODO: 452
    PASSWORD_INCORRECT(452, "Password is incorrect"),

    //TODO: 453 Disabled
    USER_DISABLED(453, "User account is disabled"),
    ;
    private final String message;

    private final int code;

    StatusCode(final int code, final String message) {

        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public int getCode() {
        return code;
    }
}
