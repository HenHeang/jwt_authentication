package com.example.jwt_authentication.enums;

import com.example.jwt_authentication.helper.GenericEnum;
import com.example.jwt_authentication.helper.converter.AbstractEnumConverter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;



public enum RoleType implements GenericEnum<RoleType, String> {

    ADMIN("ADMIN"),
    MODERATOR("MODERATOR"),
    USER("USER"),
    HR("HR"),
    EMPLOYEE("EMPLOYEE")
    ;

    private final String value;

    private RoleType(String value) {
        this.value = value;
    }


    @JsonCreator
    public static RoleType fromValue(String value) {
        for(RoleType my: RoleType.values()) {
            if(my.value.equals(value)) {
                return my;
            }
        }

        return null;
    }


    @JsonValue
    public String getValue() {
        return value;
    }


    @Override
    public String getLabel() {
        String label = "(no label)";

        if("ADMIN".equals(value)) label = "admin";
        else if("HR".equals(value)) label = "hr";
        else if("EMPLOYEE".equals(value)) label = "employee";

        return label;
    }

    public static class Converter extends AbstractEnumConverter<RoleType, String> {

        public Converter() {
            super(RoleType.class);
        }

    }

}
