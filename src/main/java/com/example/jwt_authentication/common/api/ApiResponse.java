package com.example.jwt_authentication.common.api;

import com.example.jwt_authentication.enums.StatusCode;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude
public class ApiResponse<T> {
    @JsonProperty("status")
    private ApiStatus apiStatus;

    private T data;

    public ApiResponse(T data) {
        this.data = data;
    }

    public ApiResponse(ApiStatus apiStatus, T data) {
        this.apiStatus = apiStatus;
        this.data = data;
    }

    @Builder
    public ApiResponse(StatusCode apiStatus, T data){
        this.apiStatus = new ApiStatus(apiStatus);
        this.data = data;
    }
}
