package com.example.jwt_authentication.common;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public record EmptyJsonResponse() {


}
