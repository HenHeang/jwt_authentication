package com.example.jwt_authentication.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<Long, User> {
    User findByUsername(String username);

    Optional<User> findUserByUsername(String username);

}
