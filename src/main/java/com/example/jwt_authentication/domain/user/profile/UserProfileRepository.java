package com.example.jwt_authentication.domain.user.profile;


import com.example.jwt_authentication.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {

    UserProfile findUserProfileByUser(User user);

}
