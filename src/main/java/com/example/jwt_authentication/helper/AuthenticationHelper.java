package com.example.jwt_authentication.helper;

import com.example.jwt_authentication.domain.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuthenticationHelper {
    private static Authentication getAuth() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    private static User getUser() {
        var authentication = getAuth();
        if(authentication.getPrincipal() instanceof User) {
            var user = ((User) getAuth().getPrincipal());
            return user;
        }
        return null;
    }

    public static Long getUserId() {
        return getUser().getId();
    }

    public static String getUsername() {
        return getAuth().getName();
    }
}
