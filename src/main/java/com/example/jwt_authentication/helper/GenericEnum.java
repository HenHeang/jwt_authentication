package com.example.jwt_authentication.helper;

public interface GenericEnum <T, E>{
    E getValue();
    String getLabel();
}
