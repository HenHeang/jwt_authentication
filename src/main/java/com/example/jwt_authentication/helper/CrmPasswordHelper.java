package com.example.jwt_authentication.helper;


import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Base64;

@Component
public class CrmPasswordHelper {
    private static Cipher cipher;
    private static String iv;

    private static SecretKeySpec secretKeySpec;

    private static IvParameterSpec ivspec;

    static {
        try {
            byte[] secretKey = getKey("groupwareweb");
            iv = new String(Hex.encode(secretKey)).substring(0, 16);
            secretKeySpec = new SecretKeySpec(secretKey, "AES");
            ivspec = new IvParameterSpec(iv.getBytes());
            cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private static byte[] getKey(String originalKey) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] key = digest.digest(originalKey.getBytes()); //32 bytes
        return key;
    }

    public static String encrypt(String plainText) {
        try {

            byte[] dataBytes = plainText.getBytes();
            int plaintextLength = dataBytes.length;
            byte[] plaintext = new byte[plaintextLength];
            System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);

            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivspec);

            byte[] encrypted = cipher.doFinal(plaintext);
            return new String(Base64.getEncoder().encode(encrypted));

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String decrypt(String encryptedText) throws Exception {
        byte[] encrypted = Base64.getDecoder().decode(encryptedText);

        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivspec);

        byte[] original = cipher.doFinal(encrypted);
        return new String(original);
    }

}
