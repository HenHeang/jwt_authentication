package com.example.jwt_authentication.config;


import com.example.jwt_authentication.component.LongRequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
    private final LongRequestInterceptor logRequestInterceptor;

    @Autowired
    public MvcConfig(LongRequestInterceptor logRequestInterceptor) {
        this.logRequestInterceptor = logRequestInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(logRequestInterceptor)
                .addPathPatterns("/api/**");

    }
}
