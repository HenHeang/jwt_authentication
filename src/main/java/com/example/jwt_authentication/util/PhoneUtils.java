package com.example.jwt_authentication.util;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class PhoneUtils {

    final public static String DEFAULT_COUNTRY_CODE_NUMERIC= "855";
    final public static String DEFAULT_COUNTRY_CODE_ALPHA2 = "KH";

    public static String formatPhoneNumber(String number){
        try {
            PhoneNumber phoneNumber = PhoneNumberUtil.getInstance().parse(number, "KH");
            String phone = PhoneNumberUtil.getInstance().format(phoneNumber, PhoneNumberFormat.NATIONAL);
            return phone.replaceAll("\\s", "");
        } catch (NumberParseException e) {
          return number;
        }
    }

    // https://www.programcreek.com/java-api-examples/index.php?api=com.google.i18n.phonenumbers.PhoneNumberUtil
    public static PhoneNumber parseNumber(String str, String countryCode) {
        try {
//            str = normalizeNumber(str, countryCode);
//            AppLogManager.debug(str);
//
            return PhoneNumberUtil.getInstance().parse(str, countryCode);
        } catch (NumberParseException e) {
            AppLogManager.error(e);
        }

        return null;
        /*
        if (StringUtils.isBlank(str)) {
            return null;
        }

        PhoneNumberUtil numberUtil = PhoneNumberUtil.getInstance();
        PhoneNumber result;
        try {
            result = numberUtil.parse(str, countryCode);
            if (result == null) {
                return null;
            }
        } catch (NumberParseException localNumberParseException) {
            return null;
        }
        return result;
        */
    }

    public static PhoneNumber parseNumber(String str) {
        return parseNumber(str, "");
    }

    /*
    public static String normalizeNumber(String number) {
        try {
            PhoneNumber phoneNum = parseNumber(number);

            if(phoneNum == null) {
                phoneNum = parseNumber(number, "");
            }

//			String countryCode = StringUtils.defaultIfBlank(PhoneNumberUtil.getInstance().getRegionCodeForNumber(phoneNum), DEFAULT_COUNTRY_CODE_ALPHA2);

            if(phoneNum != null) {
                return normalizeNumber(phoneNum);
            }

//			PhoneNumber phoneNum = PhoneUtils.parseNumber(number);
//
//			if(phoneNum == null) {
//				phoneNum = PhoneUtils.parseNumber(number, PhoneUtils.DEFAULT_COUNTRY_CODE_ALPHA2);
//			}
//
//			return PhoneUtils.(phoneNum, PhoneNumberFormat.INTERNATIONAL);
        }
        catch(Exception e) {
            AppLogManager.error(e);
        }

//		return number;
        return null;
    }
    */

    public static String normalizeNumber(PhoneNumber phoneNum) {
        return "+" + formatNumberDigitsOnly(phoneNum, PhoneNumberFormat.INTERNATIONAL);
    }

    public static String normalizeNumber(String number) {
        return normalizeNumber(number, "");
    }

    public static String normalizeNumber(String number, String countryCode) {
        return "+" + formatNumberDigitsOnly(number, PhoneNumberFormat.INTERNATIONAL, countryCode);

        /*
		number = number.toLowerCase();

		// if the number ends with e11, then that is Excel corrupting it, remove it
		if (number.endsWith("e+11") || number.endsWith("e+12")) {
			number = number.substring(0, number.length() - 4).replace(".", "");
		}

		// remove other characters
		number = number.replaceAll("[^0-9a-z\\+]", "");

		// add on a plus if it looks like it could be a fully qualified number
		if (number.length() > 11 && number.charAt(0) != '+') {
			number = '+' + number;
		}

		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		Phonenumber.PhoneNumber normalized = null;
		try {
			if(countryCodeMap.containsKey(countryCode)) {
				normalized = phoneUtil.parse(number, countryCodeMap.get(countryCode));
			}
			else {
				normalized = phoneUtil.parse(number, countryCode);
			}
		} catch (NumberParseException e) {
		}

		// now does it look plausible ?
		try {
			if (phoneUtil.isValidNumber(normalized)) {
				return phoneUtil.format(normalized, PhoneNumberUtil.PhoneNumberFormat.E164);
			}
		} catch (NullPointerException ex) {
		}

		// this must be a local number of some kind, just lowercase and save
		return number.replaceAll("[^0-9a-z]", "");
		*/
    }

    /*
	public static boolean isValidPhoneNumber(String str) {
		return isValidPhoneNumber(str, "");
	}
	*/

    public static boolean isValidNumberForRegion(PhoneNumber phoneNumber,String countryCode){
        try {
            return PhoneNumberUtil.getInstance().isValidNumberForRegion(phoneNumber, countryCode);
        }
        catch(Exception e) {
            AppLogManager.error(e);
        }

        return false;
    }

    public static boolean isValidNumber(String str) {
        return isValidNumber(str, "");
    }

    public static boolean isValidNumber(String str, String countryCode) {
        try {
            PhoneNumber phoneNumber = parseNumber(str, countryCode);
            return PhoneNumberUtil.getInstance().isValidNumber(phoneNumber);
        }
        catch(Exception e) {
            AppLogManager.error(e);
        }

        return false;
    }

    public static String formatNumberDigitsOnly(String number, PhoneNumberFormat numberFormat) {
        String countryCode = StringUtils.defaultIfBlank(PhoneNumberUtil.getInstance().getRegionCodeForNumber(parseNumber(number)), DEFAULT_COUNTRY_CODE_ALPHA2);

        if(PhoneUtils.isValidNumber(number, countryCode)) {
            return formatNumberDigitsOnly(number, numberFormat, countryCode);
        }

        return number;
    }

    public static String formatNumber(String number) {
        if(StringUtils.startsWith(number, "+")) {
            return formatNumber(number, PhoneNumberFormat.INTERNATIONAL);
        }
        else {
            return formatNumber(number, PhoneNumberFormat.INTERNATIONAL, DEFAULT_COUNTRY_CODE_ALPHA2);
        }
    }

    public static String formatNumber(String number, PhoneNumberFormat numberFormat) {
        String countryCode = StringUtils.defaultIfBlank(PhoneNumberUtil.getInstance().getRegionCodeForNumber(parseNumber(number)), DEFAULT_COUNTRY_CODE_ALPHA2);

        if(PhoneUtils.isValidNumber(number, countryCode)) {
            return formatNumber(number, numberFormat, countryCode);
        }

        return number;
    }

    public static String formatNumber(String number, PhoneNumberFormat numberFormat, String countryCode)
    {
        PhoneNumber phoneNumber = parseNumber(number, countryCode);

        String formatted = null;

        if(phoneNumber != null) {
            formatted = PhoneNumberUtil.getInstance().format(phoneNumber, numberFormat);
        }

        String digitOnly = StringUtils.defaultString(formatted, number);
        return addZeroToNumber(digitOnly);
    }

    public static String formatNumber(PhoneNumber phoneNumber, PhoneNumberFormat numberFormat)
    {
        String digitsOnly = PhoneNumberUtil.getInstance().format(phoneNumber, numberFormat);
        return addZeroToNumber(digitsOnly);
    }

    public static String addZeroToNumber(String phoneNumber){
//        if(!StringUtils.startsWith(phoneNumber,"0"))
//            phoneNumber = "0" + phoneNumber;
        return phoneNumber;
    }

    public static String formatNumberDigitsOnly(String number, PhoneNumberFormat numberFormat, String countryCode)
    {
        return PhoneNumberUtil.normalizeDigitsOnly(formatNumber(number, numberFormat, countryCode));
    }

    public static String formatNumberDigitsOnly(PhoneNumber phoneNumber, PhoneNumberFormat numberFormat)
    {
        return PhoneNumberUtil.normalizeDigitsOnly(formatNumber(phoneNumber, numberFormat));
    }

    public static String getCountryCodeAlpha2(String num) {
        return getCountryCodeAlpha2(parseNumber(num));
    }

    public Integer getCountryCodeNumeric(String num) {
        return getCountryCodeNumeric(parseNumber(num));
    }

    public static String getCountryCodeAlpha2(PhoneNumber phoneNumber) {
        if(phoneNumber != null) {
            return PhoneNumberUtil.getInstance().getRegionCodeForNumber(phoneNumber);
        }
        return null;
    }

    public static Integer getCountryCodeNumeric(PhoneNumber phoneNumber) {
        if(phoneNumber != null) {
            return phoneNumber.getCountryCode();
        }
        return null;
    }

    public static boolean isPhoneNumber(String input) {
        // Define a regex pattern for a simple phone number (change as needed)
        String phoneRegex = "\\d{12,13}"; // This is a simple pattern for a 10-digit phone number

        Pattern pattern = Pattern.compile(phoneRegex);
        Matcher matcher = pattern.matcher(input);

        return matcher.matches();
    }

    public static boolean isEmail(String input) {
        // Define a regex pattern for a simple email address (change as needed)
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";

        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(input);

        return matcher.matches();
    }


    /*
	public static boolean isValidPhoneNumber(String str, String countryCode) {

		str = str.replaceAll("\\D+", "");

		countryCode = defaultIfBlank(countryCode, "KR");

		//RegexValidator validator = new RegexValidator("^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?[0-9]{3,4}-?[0-9]{4}$");
		//return validator.isValid(str);

		if("KR".equals(countryCode)) {
			if(str.startsWith("010") & str.length() != 11) {
				return false;
			}
		}

		try {
			PhoneNumber phoneNumber = PhoneNumberUtil.getInstance().parse(str, countryCode);
			return PhoneNumberUtil.getInstance().isValidNumber(phoneNumber);
		}
		catch(Exception e) {
			return false;
		}
	}
	*/

    public static void main(String[] args) {
        PhoneNumber phone = PhoneUtils.parseNumber("012333444", "KH");

        System.err.println(phone);

        System.err.println(PhoneUtils.formatNumberDigitsOnly(phone, PhoneNumberFormat.E164));
    }
}
