package com.example.jwt_authentication.util;

public class SnakeCaseUtils {

    public static String toSnakeCase(String input) {

        if (input == null || input.isEmpty()) {
            return "";
        }

        // Replace uppercase letters with underscores followed by lowercase letters
        String snakeCase = input.replaceAll("([a-z])([A-Z])", "$1_$2");

        // Convert to lowercase
        return snakeCase.toLowerCase();
    }

}
