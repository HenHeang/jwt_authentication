package com.example.jwt_authentication.util;

import org.springframework.data.jpa.domain.Specification;

public abstract class BaseSpecification<T, U> {

    public static final String wildcard = "%";

    public abstract Specification<T> getFilter(U request);

    protected String containsLowerCase(String searchField) {
        return wildcard + searchField.toLowerCase() + wildcard;
    }

    public static String containLowerCase(String searchField) {
        return wildcard + searchField.toLowerCase() + wildcard;
    }
    public static String containUpperCase(String searchField) {
        return wildcard + searchField.toUpperCase() + wildcard;
    }
}
