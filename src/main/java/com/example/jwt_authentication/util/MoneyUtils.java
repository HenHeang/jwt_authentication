package com.example.jwt_authentication.util;

import org.apache.commons.lang3.ObjectUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class MoneyUtils {

    public static BigDecimal round(double value) {
        var roundedValue = Math.floor((value * Math.pow(10, 2)) + 0.5) * Math.pow(10, -2);
        return new BigDecimal(roundedValue).setScale(2, RoundingMode.HALF_UP);
    }

    public static BigDecimal convertToBigDecimal(String amount) {
        return new BigDecimal(amount);
    }

    public static boolean isNotZero(BigDecimal amount) {
        return !isZero(amount);
    }

    public static boolean isZero(BigDecimal amount) {
        return ( amount == null || amount.compareTo(BigDecimal.ZERO) == 0);
    }

    public static boolean isPositive(BigDecimal amount) {
        return ( amount != null && amount.compareTo(BigDecimal.ZERO) > 0);
    }

    public static boolean isEquals(BigDecimal amount1, BigDecimal amount2) {
        return (amount1 == null && amount2 == null) || (amount1 != null && amount2 != null && amount1.compareTo(amount2) == 0);
    }

    public static boolean isNotEquals(BigDecimal amount1, BigDecimal amount2) {
        return !isEquals(amount1, amount2);
    }

    public static boolean isBetween(BigDecimal amount, BigDecimal min, BigDecimal max) {
        return (amount != null && amount.compareTo(min) >= 0 && amount.compareTo(max) <= 0);
    }

    public static boolean isLessThan(BigDecimal amount, BigDecimal max) {
        return (amount != null && amount.compareTo(max) < 0);
    }

    public static boolean isLessThanOrEquals(BigDecimal amount, BigDecimal max) {
        return (amount != null && amount.compareTo(max) <= 0);
    }

    public static boolean isGreaterThan(BigDecimal amount, BigDecimal min) {
        return (amount != null && amount.compareTo(min) > 0);
    }

    public static boolean isGreaterThanOrEquals(BigDecimal amount, BigDecimal min) {
        return (amount != null && amount.compareTo(min) >= 0);
    }

    public static String formatMoney(BigDecimal money, String currency)
    {
        if(ObjectUtils.isEmpty(money)) return "0";

        String pattern="###,###.##";
        DecimalFormat myFormatter = new DecimalFormat(pattern);
        String fmoney = "";
        fmoney = myFormatter.format(money);

        if(currency.equals("USD")){
            currency = "$ ";
        }else if (currency.equals("KHR")){
            currency = "៛ ";
        }

        return currency.concat(fmoney);
    }

    public static String formatMoneys(BigDecimal money, String currency)
    {
        if(ObjectUtils.isEmpty(money)) return "0";

        NumberFormat formatter = NumberFormat.getNumberInstance(Locale.US);
        formatter.setMinimumFractionDigits(2);
        formatter.setMaximumFractionDigits(2);

        NumberFormat formatter1 = NumberFormat.getNumberInstance(Locale.US);
        formatter1.setMinimumFractionDigits(0);
        formatter1.setMaximumFractionDigits(0);
        String fmoney = "";
        if (currency.equals("USD")) {
            fmoney = formatter.format(money);
        } else {
            fmoney = formatter1.format(money);
        }

        if(currency.equals("USD")){
            currency = "$ ";
        }else if (currency.equals("KHR")){
            currency = "៛ ";
        }

        return currency.concat(fmoney);
    }

    public static String getPercentage(BigDecimal fee){
        if(ObjectUtils.isEmpty(fee)) return "0";

        String pattern="###,###.##";
        DecimalFormat myFormatter = new DecimalFormat(pattern);

        return myFormatter.format(fee).concat("%");
    }

    public static String formatCurrency(String currency)
    {
        if(currency.equals("USD")){
            currency = "$ ";
        }else if (currency.equals("KHR")){
            currency = "៛ ";
        }

        return currency;
    }

    public static String priceWithDecimal (BigDecimal price) {
        NumberFormat formatter = NumberFormat.getNumberInstance(Locale.US);
        formatter.setMinimumFractionDigits(2);
        formatter.setMaximumFractionDigits(2);
        return formatter.format(price);
    }

    public static String priceWithoutDecimal (BigDecimal price) {
        NumberFormat formatter = NumberFormat.getNumberInstance(Locale.US);
        formatter.setMinimumFractionDigits(0);
        formatter.setMaximumFractionDigits(0);
        return formatter.format(price);
    }

    public static void main(String[] args) {
        //System.err.println(isEquals(new BigDecimal("100"), new BigDecimal("100.00")));
        //System.err.println(formatMoney(new BigDecimal("555555"), "KHR"));
    }

}
