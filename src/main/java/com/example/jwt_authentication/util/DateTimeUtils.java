package com.example.jwt_authentication.util;

import org.springframework.lang.NonNull;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.TimeZone;

public class DateTimeUtils {

    private static final String VA_PATTERN_YYYYMM = "yyyyMM";
    private static final String VA_PATTERN_TIME6 = "HHmmss";
    private static final String VA_PATTERN_MONTH6 = "yyyyMM";
    private static final String VA_PATTERN_DATE8 = "yyyyMMdd";
    private static final String VA_PATTERN_DTM14 = "yyyyMMddHHmmss";
    private static final String VA_PATTERN_DTM_MINUTE = "yyyyMMddHHmm";

    public static final DateTimeFormatter VA_FORMATTER_TIME6 = DateTimeFormatter.ofPattern(VA_PATTERN_TIME6);
    public static final DateTimeFormatter VA_FORMATTER_DATE8 = DateTimeFormatter.ofPattern(VA_PATTERN_DATE8);
    public static final DateTimeFormatter VA_FORMATTER_DTM14 = DateTimeFormatter.ofPattern(VA_PATTERN_DTM14);
    public static final DateTimeFormatter VA_FORMATTER_MONTH6= DateTimeFormatter.ofPattern(VA_PATTERN_MONTH6);
    public static final DateTimeFormatter VA_FORMATTER_YYYYMM = DateTimeFormatter.ofPattern(VA_PATTERN_YYYYMM);

    public static final DateTimeFormatter VA_FORMATTER_DTM_MINUTE = DateTimeFormatter.ofPattern(VA_PATTERN_DTM_MINUTE);

    public static final Clock clock = Clock.system(TimeZone.getDefault().toZoneId());
    public static final Duration clockSkew = Duration.ofSeconds(60);

    public static LocalDateTime ictNow() {
        return LocalDateTime.now(clock);
    }

    public static String getDateNow(){
        return ictNow().format(VA_FORMATTER_DATE8);
    }

    public static String getTimeNow(){
        return ictNow().format(VA_FORMATTER_TIME6);
    }

    public static String getDateTimeNow(){
        return ictNow().format(VA_FORMATTER_DTM14);
    }

    public static LocalDateTime atEndOfDay(){
        return ictNow().toLocalDate().atTime(LocalTime.MAX);
    }

    public static LocalDateTime prevNow() {
        var localDateTime = ictNow();
        return localDateTime.minusDays ( 1 );
    }

    public static String getDueDateTime(Long term) {
        return ictNow().plusDays(term).format(VA_FORMATTER_DTM14);
    }

    public static LocalDateTime parseDateTime(@NonNull String dateTime) {
        return LocalDateTime.parse(dateTime, VA_FORMATTER_DTM14);
    }

    public static LocalDateTime formatDateTimeMinute(@NonNull String dateTime) {
        return LocalDateTime.parse(dateTime , VA_FORMATTER_DTM_MINUTE);
    }

    public static LocalDate parseDate(String date) {
        if(date == null || date.length() != 8) {
            return null;
        }
        return LocalDate.parse(date, VA_FORMATTER_DATE8);
    }

    public static boolean isWeekend(LocalDate date) {
        DayOfWeek day = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        return day == DayOfWeek.SUNDAY || day == DayOfWeek.SATURDAY;
    }

    public static void main(String[] args) {
        System.err.println(parseDateTime("20230227125500").isAfter(LocalDateTime.now()));
        System.err.println(DateTimeUtils.ictNow().format(VA_FORMATTER_YYYYMM).equals(parseDateTime("20220627125500").format(VA_FORMATTER_YYYYMM)));

    }

    public static String stringToDate (String value) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        return LocalDate.parse(value, formatter).format(DateTimeFormatter.ofPattern("dd/MMMM/yyyy"));
    }

    public static String stringToDateWithFormat (String value) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        return LocalDate.parse(value, formatter).format(DateTimeFormatter.ofPattern("dd, MMMM yyyy"));
    }

    public static String stringToDateYYYY_MM_DD (String value) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        return LocalDate.parse(value, formatter).format(DateTimeFormatter.ofPattern("YYYY-MM-dd"));
    }

    public static String stringToDateYYYYMMDD (String value) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        return LocalDate.parse(value, formatter).format(DateTimeFormatter.ofPattern(VA_PATTERN_DATE8));
    }
}
