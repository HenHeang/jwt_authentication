package com.example.jwt_authentication.util;


import org.apache.commons.lang3.StringUtils;

public class ImageUtils {

    public static FileInfoConfig fileInfoConfig;

    public static String getImageUrl(String imageUrl) {

        if(StringUtils.isBlank(imageUrl)) return "";

        return fileInfoConfig.getBaseUrl() + "/" + imageUrl;
    }

    public static String getImageUrlWithFallback(String imageUrl) {

        if(StringUtils.isBlank(imageUrl)) return fileInfoConfig.getBaseUrl() + "/user-default.png";

        return fileInfoConfig.getBaseUrl() + "/" + imageUrl;
    }
}
