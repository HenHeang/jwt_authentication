package com.example.jwt_authentication.properties;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

public record RsaKeysProperties(

        RSAPublicKey publicKey,
        RSAPrivateKey privateKey
) {
}
