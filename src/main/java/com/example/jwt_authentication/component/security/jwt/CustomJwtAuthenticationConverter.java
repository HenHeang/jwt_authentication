package com.example.jwt_authentication.component.security.jwt;

import com.example.jwt_authentication.domain.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class CustomJwtAuthenticationConverter implements Converter<OAuth2ResourceServerProperties.Jwt, AbstractAuthenticationToken> {

    private final UserRepository userRepository;

    @Override
    public AbstractAuthenticationToken convert(OAuth2ResourceServerProperties.Jwt jwt) {

        var user = userRepository.findUserByUsername(jwt.getSubject())
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));

        return new UserJwtAuthenticationToken<>(jwt, user);
    }
}