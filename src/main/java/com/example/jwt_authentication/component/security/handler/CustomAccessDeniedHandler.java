package com.example.jwt_authentication.component.security.handler;


import com.example.jwt_authentication.common.EmptyJsonResponse;
import com.example.jwt_authentication.common.api.ApiResponse;
import com.example.jwt_authentication.common.api.ApiStatus;
import com.example.jwt_authentication.enums.StatusCode;
import com.example.jwt_authentication.util.ObjectUtils;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    /**
     * Handles an access denied failure.
     * @param request               that resulted in an <code>AccessDeniedException</code>
     * @param response              so that the user agent can be advised of the failure
     * @param accessDeniedException that caused the invocation
     * @throws IOException      in the event of an IOException
     * @throws ServletException in the event of a ServletException
     */
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException {

        ServletServerHttpResponse res = new ServletServerHttpResponse(response);
        res.setStatusCode(HttpStatus.FORBIDDEN);
        res.getServletResponse().setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        StatusCode statusCode = StatusCode.FORBIDDEN;
        ApiStatus apiStatus = new ApiStatus(statusCode);
        apiStatus.setMessage(accessDeniedException.getMessage());

        ApiResponse<Object> apiResponse = new ApiResponse<>(apiStatus, new EmptyJsonResponse());

        res.getBody().write(ObjectUtils.writeValueAsString(apiResponse).getBytes());
    }
}


