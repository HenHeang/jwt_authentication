package com.example.jwt_authentication.component.security.handler;

import com.example.jwt_authentication.common.EmptyJsonResponse;
import com.example.jwt_authentication.common.api.ApiResponse;
import com.example.jwt_authentication.common.api.ApiStatus;
import com.example.jwt_authentication.enums.StatusCode;
import com.example.jwt_authentication.util.ObjectUtils;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import java.io.IOException;
import java.io.Serializable;

public class CustomUnauthorizedHandler implements AuthenticationEntryPoint, Serializable {
    /**
     * Commences an authentication scheme.
     * <p>
     * <code>ExceptionTranslationFilter</code> will populate the <code>HttpSession</code>
     * attribute named
     * <code>AbstractAuthenticationProcessingFilter.SPRING_SECURITY_SAVED_REQUEST_KEY</code>
     * with the requested target URL before calling this method.
     * <p>
     * Implementations should modify the headers on the <code>ServletResponse</code> as
     * necessary to commence the authentication process.
     *
     * @param request       that resulted in an <code>AuthenticationException</code>
     * @param response      so that the user agent can begin authentication
     * @param authException that caused the invocation
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {

        ServletServerHttpResponse res = new ServletServerHttpResponse(response);
        res.setStatusCode(HttpStatus.UNAUTHORIZED);
        res.getServletResponse().setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        StatusCode statusCode = StatusCode.UNAUTHORIZED;
        ApiStatus apiStatus = new ApiStatus(statusCode);
//        apiStatus.setMessage(authException.getMessage());
        apiStatus.setMessage("Unauthorized");

        ApiResponse<Object> apiResponse = new ApiResponse<>(apiStatus, new EmptyJsonResponse());

        res.getBody().write(ObjectUtils.writeValueAsString(apiResponse).getBytes());
    }
}
