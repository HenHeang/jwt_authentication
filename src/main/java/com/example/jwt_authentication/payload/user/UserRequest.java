package com.example.jwt_authentication.payload.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.util.Date;

public class UserRequest {
    @NotBlank
    private String username;

    @NotBlank
    private String email;

    @NotBlank
    private String password;


    @NotBlank
    @JsonProperty("full_name")
    private String fullName;;


    @NotNull
    @Temporal(TemporalType.DATE)
    @JsonProperty("date_of_birth")
    private Date dateOfBirth;

    @NotBlank
    private String address;

    @JsonProperty("image_url")
    private String imageUrl;
}
