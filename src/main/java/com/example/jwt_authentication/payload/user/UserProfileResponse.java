package com.example.jwt_authentication.payload.user;

import com.example.jwt_authentication.common.EmptyJsonResponse;
import com.example.jwt_authentication.domain.User;
import com.example.jwt_authentication.util.ObjectUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class UserProfileResponse {
    private Long id;

    private String username;

    private String email;

    private String role;

    @JsonInclude
    private String fullName;

    @JsonInclude
    private Date dateOfBirth;

    @JsonInclude
    private String address;

    @JsonInclude
    private String imageUrl;

    public UserProfileResponse(User payload) {

        if (ObjectUtils.anyNotNull(payload)) {

            this.id = payload.getId();
            this.username = payload.getUsername();
            this.email = payload.getEmail();
            this.role = payload.getRole().getName();

            if (ObjectUtils.anyNull(payload.getUserProfile())) {

                payload.setUserProfile(new UserProfile());
            } else {

                this.fullName = payload.getUserProfile().getFullName();
                this.dateOfBirth = payload.getUserProfile().getDateOfBirth();
                this.address = payload.getUserProfile().getAddress();
                this.imageUrl = payload.getUserProfile().getImageUrl();
            }
        } else {

            new EmptyJsonResponse();
        }
    }
}
