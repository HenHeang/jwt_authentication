package com.example.jwt_authentication.payload.user;

import com.example.jwt_authentication.domain.User;
import com.example.jwt_authentication.payload.common.Pagination;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@NoArgsConstructor
public class UserMainResponse {

    @JsonProperty("users")
    private List<UserProfileResponse> userResponses;

    @JsonProperty("pagination")
    private Pagination pagination;

    public UserMainResponse(Page<User> page) {
        this.userResponses = page.getContent().stream().map(UserProfileResponse::new).collect(Collectors.toList());
        this.pagination = new Pagination(page);
    }

}
