package com.example.jwt_authentication.payload.common.datetime;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.ZonedDateTime;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
public abstract class UpdatableEntity extends CreatableEntity {

    @JsonProperty("updated_at")
    @LastModifiedDate
    @Column(nullable = false)
    private ZonedDateTime updatedAt;

}
