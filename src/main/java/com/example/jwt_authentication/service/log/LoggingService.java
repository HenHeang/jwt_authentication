package com.example.jwt_authentication.service.log;


import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Service;

@Service
public interface LoggingService {
    void logRequest(HttpServletRequest httpServletRequest, Object body);

}
